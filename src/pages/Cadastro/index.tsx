import React from 'react';
import { View, } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {MainText, ImageBackgroundApp, SubmitButton, Input, SubmitButtonText} from "./styles";
import { Container, Content, Title, InputBox, InputLabel, BackIcon } from '../styles';


import { useForm, Controller } from 'react-hook-form';
import { RadioButton } from 'react-native-paper';

import Icon from 'react-native-vector-icons/Feather';



interface RegisterData {
    name: string,
    email: string,
    birthdate: string,
    gender: string,
    password: string,
    passwordConfirmation: string,
}

export default function Register() {
    const { control, getValues, handleSubmit, } = useForm({ mode: 'onTouched' });
    const onSubmit = (data: RegisterData) => {
        console.log(data)
        };
    const onError = (errors: Object) => { console.log(errors) };
    const navigation = useNavigation();

    return (
        <ImageBackgroundApp source={require('../../assets/foto.png')} style={{flex: 1}}>
        <Container>
            <BackIcon style={{top:20}} onPress={() => navigation.navigate('Login')}>
                <Icon name="arrow-left-circle" size={35} color="#32CFE3" />
            </BackIcon>
            
            <Content>
                <MainText>Cadastro</MainText>
                <InputBox>
                    <InputLabel>Nome</InputLabel>
                    <Controller
                        control={control}
                        render={({field:{onBlur,onChange, value}}) => (
                            <Input
                                autoCompleteType='name'
                                autoCorrect={false}
                                textContentType='name'
                                onBlur={onBlur}
                                onChangeText={(value:any) => onChange(value)}
                                value={value}
                            />
                        )}
                        rules={{ required: 'O nome é obrigatório.' }}
                        name='name'
                        defaultValue=''
                    />
                </InputBox>

                <InputBox>
                    <InputLabel>E-mail</InputLabel>
                    <Controller
                        control={control}
                        render={({field:{onBlur,onChange, value}}) => (
                            <Input
                                autoCompleteType='email'
                                autoCorrect={false}
                                keyboardType='email-address'
                                textContentType='emailAddress'
                                onBlur={onBlur}
                                onChangeText={(value:any) => onChange(value)}
                                value={value}
                            />
                        )}
                        rules={{
                            required: 'O e-mail é obrigatório.',
                            pattern: {
                                value: /^\S+@\S+$/i,
                                message: 'Formato de e-mail inválido.'
                            },
                        }}
                        name='email'
                        defaultValue=''
                    />
                </InputBox>

                <InputBox>
                    <InputLabel>Data de Nascimento</InputLabel>
                    <Controller
                        control={control}
                        render={({field:{onBlur,onChange, value}}) => (
                            <Input
                                placeholder='DD/MM/AAAA'
                                type={'datetime'}
                                keyboardType='numeric'
                                onBlur={onBlur}
                                options={{
                                    format: 'DD/MM/YYYY'
                                }}
                                value={value}
                                onChangeText={(value) => onChange(value)}
                                style={{
                                    width: '100%',
                                    padding: '3%', paddingLeft: '5%',
                                    backgroundColor: '#fff',
                                    borderRadius: 100,
                                    shadowOffset: { width: 2, height: 2 }, shadowColor: 'black', shadowOpacity: 0.25, shadowRadius: 4,
                                }}
                            />
                        )}
                        rules={{
                            required: 'A data de nascimento é obrigatória.',
                            
                        }}
                        name='birthdate'
                        defaultValue=''
                    />
                </InputBox>

                <InputBox>
                    <InputLabel>Gênero</InputLabel>
                    <Controller
                        control={control}
                        render={({field:{onBlur,onChange, value}}) => (
                            <View>
                                <RadioButton.Group onValueChange={(value) => onChange(value)} value={value}>
                                    <RadioButton.Item color='#32CFE3' label="Feminino" value="feminino" />
                                    <RadioButton.Item color='#32CFE3' label="Masculino" value="masculino" />
                                    <RadioButton.Item color='#32CFE3' label="Outro" value="outro" />
                                </RadioButton.Group>
                            </View>
                        )}
                        rules={{ required: 'Esse campo é obrigatório.' }}
                        name='gender'
                        defaultValue=''
                    />
                </InputBox>

                <InputBox>
                    <InputLabel>Senha</InputLabel>
                    <Controller
                        control={control}
                        render={({field:{onBlur,onChange, value}}) => (
                            <Input
                                secureTextEntry
                                autoCompleteType='password'
                                autoCorrect={false}
                                textContentType='newPassword'
                                onBlur={onBlur}
                                onChangeText={(value:any) => onChange(value)}
                                value={value}
                            />
                        )}
                        rules={{ required: 'A senha é obrigatória.' }}
                        name='password'
                        defaultValue=''
                    />
                </InputBox>

                <InputBox>
                    <InputLabel>Confirmação de senha</InputLabel>
                    <Controller
                        control={control}
                        render={({field:{onBlur,onChange, value}}) => (
                            <Input
                                secureTextEntry
                                autoCompleteType='password'
                                autoCorrect={false}
                                textContentType='password'
                                onBlur={onBlur}
                                onChangeText={(value:any) => onChange(value)}
                                value={value}
                            />
                        )}
                        rules={{
                            required: 'A confirmação de senha é obrigatória.',
                            validate: {
                                matchesPreviousPassword: (value) => {
                                    const { password } = getValues();
                                    return password === value || 'As senhas não coincidem.';
                                }
                            }
                        }}
                        name='passwordConfirmation'
                        defaultValue=''
                    />
                </InputBox>
                
                <SubmitButton>
                    <SubmitButton onPress={handleSubmit(onSubmit, onError)}>
                        <SubmitButtonText>CADASTRAR</SubmitButtonText>
                    </SubmitButton>
                </SubmitButton>
            </Content>
            
        </Container>
        </ImageBackgroundApp>
    )
}
