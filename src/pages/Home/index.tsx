import React from 'react';
import { View, } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {LogoApp,  Bottom, TextBottom, AlignBottom, ImageBackgroundApp, MainText} from "./styles";


export default function Home(){
    const navigation = useNavigation();

    return(
        <ImageBackgroundApp source={require('../../assets/foto.png')} style={{flex: 1}}>            
            <View>
                <LogoApp source={require('../../assets/logo.png')}></LogoApp>
            </View>

            <View>
                <MainText>Seu novo amor, seu novo pet</MainText>
            </View>

            <AlignBottom>
                <Bottom onPress={() => navigation.navigate('Login')}>
                    <TextBottom>Começar</TextBottom>
                </Bottom>
            </AlignBottom>
        </ImageBackgroundApp>
    )
}