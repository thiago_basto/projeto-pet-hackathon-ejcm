import styled from 'styled-components/native';
import {widthPercentageToDP as vw, heightPercentageToDP as vh} from 'react-native-responsive-screen';


export const LogoApp = styled.ImageBackground `

    margin: auto;
    margin-top: 50%;

    resizeMode: contain;
    height: ${vh('25%')};
    width: ${vw('85%')};    
    
`;

export const Bottom = styled.TouchableOpacity `

    display:Flex;
    margin:auto;
    margin-top: 10%;
    
    height: ${vh('5%')};
    width: ${vw('45%')};
    border-radius: 10px;
    background-color: #ff6b00;

    text-align:center;
   

`;

export const ImageBackgroundApp = styled.ImageBackground `
    resizeMode: contain;  
`;

export const TextBottom = styled.Text `

    font-weight: bold;
    color: white;
    font-size: 1.3em;

`;

export const MainText = styled.Text `

    font-weight: bold;
    color: white;
    font-size: 1.1em;

    margin: auto;  

`;

export const AlignBottom = styled.View `

    display:Flex;
    justify-content:center;   
`;