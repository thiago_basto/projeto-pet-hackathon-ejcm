// import React from 'react';
// import { View, Text, ImageBackground } from 'react-native';
// import { useNavigation } from '@react-navigation/native';
// import {LogoApp,  Bottom, TextBottom, AlignBottom, ImageBackGroundApp, MainText} from "./styles";
// import { TouchableOpacity } from 'react-native-gesture-handler';


// export default function Login(){
//     const navigation = useNavigation();

//     return(
//         <ImageBackGroundApp source={require('../../assets/foto.png')} style={{flex: 1}}>            
//             <View>
//                 <LogoApp source={require('../../assets/EXEMPLO-1-LOGO.png')}></LogoApp>
//             </View>

//             {/* <View>
//                 <MainText>Seu novo amor, seu novo pet</MainText>
//             </View> */}

//             <AlignBottom>
//                 <Bottom><TextBottom>Começar</TextBottom></Bottom>
//             </AlignBottom>
//         </ImageBackGroundApp>
//     )
// }

import React, { useContext } from 'react';
import { useForm, Controller } from 'react-hook-form';


import {LogoApp,  Bottom, TextBottom, AlignBottom, ImageBackGroundApp, Container, WhiteBox, Header, Title, SubTitle, Form, InputBox, Input, ButtonContainer, LoginButton, RegisterButton, LoginText, RegisterText, BackIcon} from "./styles";

import { Text, ImageBackground, View, } from 'react-native';

import Icon from 'react-native-vector-icons/Feather';
import { useNavigation } from '@react-navigation/native';


interface FormData {
    email: string;
    password: string;
}

export default function Login() {
    

    const { control, handleSubmit} = useForm({ mode: 'onTouched' });
    const onSubmit = (data: FormData) => { 
        console.log(data)       
        }  
        

    const onError = (errors: Object) => { console.log(errors) };
    const navigation = useNavigation();
    

    return (
        <ImageBackGroundApp source={require('../../assets/foto.png')} style={{flex: 1}}>
        <Container>
            <BackIcon onPress={() => {navigation.navigate('Home');}}>
                <Icon name="arrow-left-circle" size={35} color="#ff6b00"/>
            </BackIcon>
            <WhiteBox>
                <View>
                    <LogoApp source={require('../../assets/EXEMPLO-1-LOGO.png')}></LogoApp>
                </View>
                <Form>
                    <InputBox>
                        <Controller
                            control={control}
                            render={({field:{onBlur,onChange, value}}) => (
                                <Input
                                    placeholder="E-mail"
                                    autoCompleteType='email'
                                    autoCorrect={false}
                                    keyboardType='email-address'
                                    textContentType='emailAddress'
                                    onBlur={onBlur}
                                    onChangeText={(value:any) => onChange(value)}
                                    value={value}
                                />
                            )}
                            rules={{
                                required: 'O e-mail é obrigatório.',
                                pattern: {
                                    value: /^\S+@\S+$/i,
                                    message: 'Formato de e-mail inválido.'
                                },
                            }}
                            name='email'
                            defaultValue=''
                        />
                        {/* {errors.email && <Text style={{ color: 'red' }}>{errors.email.message}</Text>} */}
                    </InputBox>
                    <InputBox>
                        <Controller
                            control={control}
                            render={({field:{onBlur,onChange, value}}) => (
                                <Input
                                    placeholder="Senha"
                                    secureTextEntry
                                    autoCompleteType='password'
                                    autoCorrect={false}
                                    textContentType='password'
                                    onBlur={onBlur}
                                    onChangeText={(value:any) => onChange(value)}
                                    value={value}
                                />
                            )}
                            rules={{ required: 'A senha é obrigatória.' }}
                            name='password'
                            defaultValue=''
                        />
                        {/* // {errors.password && <Text style={{ color: 'red' }}>{errors.password.message}</Text>} */}
                    </InputBox>
                    <ButtonContainer>
                        <RegisterButton onPress={() => navigation.navigate('Register')}>
                            <RegisterText onPress={() => navigation.navigate('Cadastro')}>cadastrar-se</RegisterText>
                        </RegisterButton>
                    </ButtonContainer>
                    <ButtonContainer>
                        <LoginButton onPress={handleSubmit(onSubmit, onError)}>
                            <LoginText>Entrar</LoginText>
                        </LoginButton>
                    </ButtonContainer>             
                </Form>
            </WhiteBox>
        </Container>
        </ImageBackGroundApp>
    );
}
